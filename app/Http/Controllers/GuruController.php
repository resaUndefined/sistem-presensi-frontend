<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $response = Http::withToken(session()->get('tokenUser'))
                            ->get(env("REST_API_ENDPOINT").'/api/guru');        
        $dataResponse = json_decode($response);  

        $this->data['dataGuru'] = $dataResponse->data;        
        
        return view('guru.index',$this->data);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = Http::withToken(session()->get('tokenUser'))
                            ->get(env("REST_API_ENDPOINT").'/api/user-untuk-guru');        
        $dataResponse = json_decode($response);  
        $this->data['users'] = $dataResponse->data;

        return view('guru.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                
        $ttl = explode('/',$request->tgl_lahir);
        $request->merge([
            'tgl_lahir' => $ttl[2].'-'.$ttl[0].'-'.$ttl[1]
        ]);
        $response = Http::withToken(session('tokenUser'))
                            ->post(
                                env("REST_API_ENDPOINT").'/api/guru',
                                $request->except('_token')
                            );        
        $data = json_decode($response);
        
        if ($data->status == true) {
            return redirect()->route('guru.index')->with('success','Data guru berhasil ditambahkan!');
        }else {
            return redirect()->route('guru.create')->with('validationErrors',$data->message);
        }
    }

    public function account_profile(Request $request)
    {
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function account()
    {
        return view('account.account');
    }

    public function account_save(Request $request)
    {
        
    }
}
